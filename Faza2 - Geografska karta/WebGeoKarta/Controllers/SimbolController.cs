﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Faza2___Geografska_karta;
using Faza2___Geografska_karta.Views;

namespace WebGeoKarta.Controllers
{
    public class SimbolController : ApiController
    {
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/simbol/")]
        public IEnumerable<Simbol> Get()
        {
            DTOWebAPI webi = new DTOWebAPI();

            IEnumerable<Simbol> ze = webi.GetSimboli();

            return ze;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/simbol/{x}")]
        public SimbolView Get(int x)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.GetTajSimbol(x);
        }

        //simbol ima konkretne vrednosti koje moze da ima za OD i DO broj stanovnika!!!
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/simbol/dodaj/")]
        public int Post([FromBody] Simbol z)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.DodajSimbol(z);
        }
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/simbol/izmeni/{id}")]
        public int Put(int id, [FromBody]Simbol z)
        {
            DTOWebAPI webi = new DTOWebAPI();

            z.ID_SIMBOL = id;

            return webi.PromeniSimbol(z);
        }
        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/simbol/obrisi/{id}")]
        public int Delete(int id)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.BrisiSimbol(id);
        }
    }
}
