﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Faza2___Geografska_karta;
using Faza2___Geografska_karta.Views;

namespace WebGeoKarta.Controllers
{
    public class PovrsinskiController : ApiController
    {
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/povrsinski/")]
        public IEnumerable<PovrsinskiObjekat> Get()
        {
            DTOWebAPI wipi = new DTOWebAPI();

            IEnumerable<PovrsinskiObjekat> poe = wipi.GetPovrsinski();

            return poe;
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/povrsinski/{x}")]
        public PovrsinskiView Get(int x)
        {
            DTOWebAPI wipi = new DTOWebAPI();

            return wipi.GetTajPovrsinski(x);
        }

        //obavezno dodati u JSON pri upisu u Postman
        //
        //     "NAZIV": "BlaBla Nesto"
        //
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/povrsinski/dodaj/")]
        public int Post([FromBody] PovrsinskiObjekat p)
        {
            DTOWebAPI wipi = new DTOWebAPI();

            return wipi.DodajPovrsinski(p);
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/povrsinski/izmeni/{id}")]
        public int Put(int id, [FromBody]PovrsinskiObjekat p)
        {
            DTOWebAPI wipi = new DTOWebAPI();

            p.ID_GOBJ = id;

            return wipi.PromeniPovrsinski(p);
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/povrsinski/obrisi/{id}")]
        public int Delete(int id)
        {
            DTOWebAPI wipi = new DTOWebAPI();

            return wipi.BrisiPovrsinski(id);
        }
    }
}
