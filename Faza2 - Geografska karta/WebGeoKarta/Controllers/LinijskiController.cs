﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Faza2___Geografska_karta;
using Faza2___Geografska_karta.Views;

namespace WebGeoKarta.Controllers
{
    public class LinijskiController : ApiController
    {
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/linijski/")]
        public IEnumerable<LinijskiObjekat> Get()
        {
            DTOWebAPI webi = new DTOWebAPI();

            IEnumerable<LinijskiObjekat> loe = webi.GetLinijski();

            return loe;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/linijski/{x}")]
        public LinijskiView Get(int x)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.GetTajLinijski(x);
        }

        //obavezno dodati u JSON pri upisu u Postman
        //
        //     "NAZIV": "BlaBla Nesto"
        //
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/linijski/dodaj/")]
        public int Post([FromBody] LinijskiObjekat l)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.DodajLinijski(l);

        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/linijski/izmeni/{id}")]
        public int Put(int id, [FromBody]LinijskiObjekat l)
        {
            DTOWebAPI webi = new DTOWebAPI();

            l.ID_GOBJ = id;

            return webi.PromeniLinijski(l);
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/linijski/obrisi/{id}")]
        public int Delete(int id)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.BrisiLinijski(id);
        }
    }
}
