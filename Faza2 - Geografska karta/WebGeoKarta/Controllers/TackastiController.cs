﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Faza2___Geografska_karta;
using Faza2___Geografska_karta.Views;

namespace WebGeoKarta.Controllers
{
    public class TackastiController : ApiController
    {
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/tackasti/")]
        public IEnumerable<TackastiObjekat> Get()
        {
            DTOWebAPI wapi = new DTOWebAPI();

            IEnumerable<TackastiObjekat> toe = wapi.GetTackasti();


            return toe;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/tackasti/{x}")]
        public TackastiView Get(int x)
        {
            DTOWebAPI wapi = new DTOWebAPI();

            return wapi.GetTajTackasti(x);
        }

        //obavezno dodati u JSON pri upisu u Postman
        //
        //     "NAZIV": "BlaBla Nesto"
        //
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/tackasti/dodaj/")]
        public int Post([FromBody] TackastiObjekat t)
        {
            DTOWebAPI wapi = new DTOWebAPI();

            return wapi.DodajTackasti(t);

        }

        //obavezno dodati u JSON pri upisu u Postman
        //
        //     "NAZIV": "ime grada ili izmenu"
        //
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/tackasti/izmeni/{id}")]
        public int Put(int id, [FromBody]TackastiObjekat t)
        {
            DTOWebAPI wapi = new DTOWebAPI();

            t.ID_GOBJ = id;

            return wapi.PromeniTackasti(t);
        }
        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/tackasti/obrisi/{id}")]
        public int Delete(int id)
        {
            DTOWebAPI wapi = new DTOWebAPI();

            return wapi.BrisiTackasti(id);
        }
    }
}
