﻿namespace Faza2___Geografska_karta
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnDodajL = new System.Windows.Forms.Button();
            this.btnDodajT = new System.Windows.Forms.Button();
            this.btnDodajP = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.btnCitajT = new System.Windows.Forms.Button();
            this.btnUpdateL = new System.Windows.Forms.Button();
            this.btnUpdateT = new System.Windows.Forms.Button();
            this.btnUpdateP = new System.Windows.Forms.Button();
            this.btnDeleteL = new System.Windows.Forms.Button();
            this.btnDeleteT = new System.Windows.Forms.Button();
            this.btnDeleteP = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCitajP = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnSledece = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDodajL
            // 
            this.btnDodajL.Location = new System.Drawing.Point(31, 3);
            this.btnDodajL.Name = "btnDodajL";
            this.btnDodajL.Size = new System.Drawing.Size(153, 23);
            this.btnDodajL.TabIndex = 0;
            this.btnDodajL.Text = "Dodaj Linijski + ostalo";
            this.btnDodajL.UseVisualStyleBackColor = true;
            this.btnDodajL.Click += new System.EventHandler(this.btnDodajL_Click);
            // 
            // btnDodajT
            // 
            this.btnDodajT.Location = new System.Drawing.Point(31, 28);
            this.btnDodajT.Name = "btnDodajT";
            this.btnDodajT.Size = new System.Drawing.Size(153, 23);
            this.btnDodajT.TabIndex = 1;
            this.btnDodajT.Text = "Dodaj Tackasti + ostalo";
            this.btnDodajT.UseVisualStyleBackColor = true;
            this.btnDodajT.Click += new System.EventHandler(this.btnDodajT_Click);
            // 
            // btnDodajP
            // 
            this.btnDodajP.Location = new System.Drawing.Point(31, 53);
            this.btnDodajP.Name = "btnDodajP";
            this.btnDodajP.Size = new System.Drawing.Size(153, 23);
            this.btnDodajP.TabIndex = 2;
            this.btnDodajP.Text = "Dodaj Povrsinski + ostalo";
            this.btnDodajP.UseVisualStyleBackColor = true;
            this.btnDodajP.Click += new System.EventHandler(this.btnDodajP_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(31, 3);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(153, 23);
            this.button8.TabIndex = 1;
            this.button8.Text = "Citaj Linijski";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.btnCitajL_Click);
            // 
            // btnCitajT
            // 
            this.btnCitajT.Location = new System.Drawing.Point(31, 28);
            this.btnCitajT.Name = "btnCitajT";
            this.btnCitajT.Size = new System.Drawing.Size(153, 23);
            this.btnCitajT.TabIndex = 2;
            this.btnCitajT.Text = "Citaj Tackasti";
            this.btnCitajT.UseVisualStyleBackColor = true;
            this.btnCitajT.Click += new System.EventHandler(this.btnCitajT_Click);
            // 
            // btnUpdateL
            // 
            this.btnUpdateL.Location = new System.Drawing.Point(31, 3);
            this.btnUpdateL.Name = "btnUpdateL";
            this.btnUpdateL.Size = new System.Drawing.Size(153, 23);
            this.btnUpdateL.TabIndex = 0;
            this.btnUpdateL.Text = "Menjaj Linijski";
            this.btnUpdateL.UseVisualStyleBackColor = true;
            this.btnUpdateL.Click += new System.EventHandler(this.btnUpdateL_Click);
            // 
            // btnUpdateT
            // 
            this.btnUpdateT.Location = new System.Drawing.Point(31, 28);
            this.btnUpdateT.Name = "btnUpdateT";
            this.btnUpdateT.Size = new System.Drawing.Size(153, 23);
            this.btnUpdateT.TabIndex = 1;
            this.btnUpdateT.Text = "Menjaj Tackasti";
            this.btnUpdateT.UseVisualStyleBackColor = true;
            this.btnUpdateT.Click += new System.EventHandler(this.btnUpdateT_Click);
            // 
            // btnUpdateP
            // 
            this.btnUpdateP.Location = new System.Drawing.Point(31, 53);
            this.btnUpdateP.Name = "btnUpdateP";
            this.btnUpdateP.Size = new System.Drawing.Size(153, 23);
            this.btnUpdateP.TabIndex = 2;
            this.btnUpdateP.Text = "Menjaj Povrsinski";
            this.btnUpdateP.UseVisualStyleBackColor = true;
            this.btnUpdateP.Click += new System.EventHandler(this.btnUpdateP_Click);
            // 
            // btnDeleteL
            // 
            this.btnDeleteL.Location = new System.Drawing.Point(31, 3);
            this.btnDeleteL.Name = "btnDeleteL";
            this.btnDeleteL.Size = new System.Drawing.Size(153, 23);
            this.btnDeleteL.TabIndex = 0;
            this.btnDeleteL.Text = "Brisi Linijski";
            this.btnDeleteL.UseVisualStyleBackColor = true;
            this.btnDeleteL.Click += new System.EventHandler(this.btnDeleteL_Click);
            // 
            // btnDeleteT
            // 
            this.btnDeleteT.Location = new System.Drawing.Point(31, 28);
            this.btnDeleteT.Name = "btnDeleteT";
            this.btnDeleteT.Size = new System.Drawing.Size(153, 23);
            this.btnDeleteT.TabIndex = 1;
            this.btnDeleteT.Text = "Brisi Tackasti";
            this.btnDeleteT.UseVisualStyleBackColor = true;
            this.btnDeleteT.Click += new System.EventHandler(this.btnDeleteT_Click);
            // 
            // btnDeleteP
            // 
            this.btnDeleteP.Location = new System.Drawing.Point(31, 53);
            this.btnDeleteP.Name = "btnDeleteP";
            this.btnDeleteP.Size = new System.Drawing.Size(153, 23);
            this.btnDeleteP.TabIndex = 2;
            this.btnDeleteP.Text = "Brisi Povrsinski";
            this.btnDeleteP.UseVisualStyleBackColor = true;
            this.btnDeleteP.Click += new System.EventHandler(this.btnDeleteP_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.LightCoral;
            this.label1.Location = new System.Drawing.Point(23, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 64);
            this.label1.TabIndex = 10;
            this.label1.Text = "C";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightCoral;
            this.panel1.Controls.Add(this.btnDodajP);
            this.panel1.Controls.Add(this.btnDodajT);
            this.panel1.Controls.Add(this.btnDodajL);
            this.panel1.Location = new System.Drawing.Point(111, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(212, 79);
            this.panel1.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.label2.Location = new System.Drawing.Point(23, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 64);
            this.label2.TabIndex = 12;
            this.label2.Text = "R";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel2.Controls.Add(this.btnCitajP);
            this.panel2.Controls.Add(this.btnCitajT);
            this.panel2.Controls.Add(this.button8);
            this.panel2.Location = new System.Drawing.Point(111, 129);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(212, 79);
            this.panel2.TabIndex = 13;
            // 
            // btnCitajP
            // 
            this.btnCitajP.Location = new System.Drawing.Point(31, 53);
            this.btnCitajP.Name = "btnCitajP";
            this.btnCitajP.Size = new System.Drawing.Size(153, 23);
            this.btnCitajP.TabIndex = 3;
            this.btnCitajP.Text = "Citaj Povrsinski";
            this.btnCitajP.UseVisualStyleBackColor = true;
            this.btnCitajP.Click += new System.EventHandler(this.btnCitajP_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Gold;
            this.label3.Location = new System.Drawing.Point(23, 239);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 64);
            this.label3.TabIndex = 14;
            this.label3.Text = "U";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gold;
            this.panel3.Controls.Add(this.btnUpdateP);
            this.panel3.Controls.Add(this.btnUpdateT);
            this.panel3.Controls.Add(this.btnUpdateL);
            this.panel3.Location = new System.Drawing.Point(111, 235);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(212, 79);
            this.panel3.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(23, 343);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 64);
            this.label4.TabIndex = 16;
            this.label4.Text = "D";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel4.Controls.Add(this.btnDeleteP);
            this.panel4.Controls.Add(this.btnDeleteT);
            this.panel4.Controls.Add(this.btnDeleteL);
            this.panel4.Location = new System.Drawing.Point(111, 338);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(212, 79);
            this.panel4.TabIndex = 17;
            // 
            // btnSledece
            // 
            this.btnSledece.Location = new System.Drawing.Point(142, 443);
            this.btnSledece.Name = "btnSledece";
            this.btnSledece.Size = new System.Drawing.Size(153, 57);
            this.btnSledece.TabIndex = 18;
            this.btnSledece.Text = "Sledeca faza";
            this.btnSledece.UseVisualStyleBackColor = true;
            this.btnSledece.Click += new System.EventHandler(this.btnSledece_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(326, 6);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 74);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(326, 106);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(93, 74);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(326, 315);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(93, 74);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 27;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(326, 211);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(93, 74);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 29;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(108, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "* za dodatni opis akcija kliknuti na oblacice";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(12, 420);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(80, 80);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 31;
            this.pictureBox5.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(94, 455);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 17);
            this.label6.TabIndex = 32;
            this.label6.Text = "----->";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 511);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSledece);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 550);
            this.MinimumSize = new System.Drawing.Size(460, 550);
            this.Name = "Form1";
            this.Text = "Geografska karta";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDodajL;
        private System.Windows.Forms.Button btnDodajT;
        private System.Windows.Forms.Button btnDodajP;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button btnCitajT;
        private System.Windows.Forms.Button btnUpdateL;
        private System.Windows.Forms.Button btnUpdateT;
        private System.Windows.Forms.Button btnUpdateP;
        private System.Windows.Forms.Button btnDeleteL;
        private System.Windows.Forms.Button btnDeleteT;
        private System.Windows.Forms.Button btnDeleteP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCitajP;
        private System.Windows.Forms.Button btnSledece;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label6;
    }
}

