﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public abstract class GeografskiObjekat
    {
        public virtual int ID_GOBJ { get; set; }
        public virtual string Naziv { get; set; }
        public virtual PovrsinskiObjekat ID_UZVISENJA { get; set; } 
        public virtual int Nadmorska_visina { get; set; }
        public virtual string TIP { get; set; }

        public GeografskiObjekat()
        {

        }
    }
}
