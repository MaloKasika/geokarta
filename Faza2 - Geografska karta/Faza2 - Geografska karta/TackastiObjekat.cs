﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Faza2___Geografska_karta
{
    public class TackastiObjekat : GeografskiObjekat
    {
        public virtual double KOORD_X { get; set; }
        public virtual double KOORD_Y { get; set; }
        public virtual DateTime DATUM_EVIDENCIJE { get; set; }
        public virtual bool NASELJENO_FLEG { get; set; }
        public virtual string OPSTINA { get; set; }
        public virtual int BROJ_STANOVNIKA { get; set; }
        public virtual DateTime DATUM_OSNIVANJA { get; set; }
        public virtual bool TURISTICKO_FLEG { get; set; }
        public virtual DateTime DATUM_TUR { get; set; }
        public virtual string TIP_TURISTICKOG { get; set; }
        public virtual Simbol ID_SIMBOLA { get; set; }

        public virtual IList<Znamenitost> ListaZnamenitosti { get; set; }
        public virtual IList<Sadrzi> ListaLinijskih { get; set; }

        public TackastiObjekat()
        {
            this.TIP = "T";

            this.NASELJENO_FLEG = false;
            this.TURISTICKO_FLEG = false;

            ListaZnamenitosti = new List<Znamenitost>();
            ListaLinijskih = new List<Sadrzi>();
        }

    }
}
