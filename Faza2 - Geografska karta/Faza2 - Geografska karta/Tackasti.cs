﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Faza2___Geografska_karta
{
    public partial class Tackasti : UserControl
    {


        public TackastiObjekat to;

        public Tackasti()
        {
            InitializeComponent();
            to = new TackastiObjekat();
            cbxZnamenitost.Checked = false;
            tbxNazivZnam.Enabled = false;
            btnDodajZnam.Enabled = false;
            listBox1.Visible = false;
            lblZnamenitost.Visible = false;
            PopuniListuL();
        }

        private void cbxNaseljeno_CheckedChanged(object sender, EventArgs e)
        {
            if (chbxNaseljeno.Checked)
            {
                to.NASELJENO_FLEG = true;

                lblBS.Enabled = true;
                lblDO.Enabled = true;
                lblOpstina.Enabled = true;
                tbxOpstina.Enabled = true;
                nudBS.Enabled = true;
                dtpOsnivanja.Enabled = true;
                chbxTuristicko.Enabled = true;
                lblNazivZnam.Enabled = true;
                cbxZnamenitost.Enabled = true;
        //        lblZnamenitost.Visible = true;
       //         listBox1.Visible = true;
       //         btnDodajZnam.Enabled = true;

            }
            else
            {
                to.NASELJENO_FLEG = false;

                lblBS.Enabled = false;
                lblDO.Enabled = false;
                lblOpstina.Enabled = false;
                tbxOpstina.Enabled = false;
                nudBS.Enabled = false;
                dtpOsnivanja.Enabled = false;
                chbxTuristicko.Enabled = false;
                lblNazivZnam.Enabled = false;
                cbxZnamenitost.Enabled = false;
                lblZnamenitost.Visible = false;
                listBox1.Visible = false;
                btnDodajZnam.Enabled = false;

            }
        }

        private void chbxTuristicko_CheckedChanged(object sender, EventArgs e)
        {
            if (chbxTuristicko.Checked)
            {
                to.TURISTICKO_FLEG = true;

                lbl1.Enabled = true;
                lbl2.Enabled = true;
                lblTipT.Enabled = true;
                dtpTur.Enabled = true;
                cbxTipT.Enabled = true;
            }
            else
            {
                to.TURISTICKO_FLEG = false;

                lbl1.Enabled = false;
                lbl2.Enabled = false;
                lblTipT.Enabled = false;
                dtpTur.Enabled = false;
                cbxTipT.Enabled = false;
            }
        }

        private void nupX_Leave(object sender, EventArgs e)
        {
            to.KOORD_X = (int)nupX.Value;
        }

        private void nupY_Leave(object sender, EventArgs e)
        {
            to.KOORD_Y = (int)nupY.Value;
        }

        private void dtpEvidencije_Leave(object sender, EventArgs e)
        {
            to.DATUM_EVIDENCIJE = dtpEvidencije.Value;
        }

        private void tbxOpstina_Leave(object sender, EventArgs e)
        {
            to.OPSTINA = tbxOpstina.Text;
        }

        private void nudBS_Leave(object sender, EventArgs e)
        {
            to.BROJ_STANOVNIKA = (int)nudBS.Value;
        }

        private void dtpOsnivanja_Leave(object sender, EventArgs e)
        {
            to.DATUM_OSNIVANJA = dtpOsnivanja.Value;
        }

        private void dtpTur_Leave(object sender, EventArgs e)
        {
            to.DATUM_TUR = dtpTur.Value;
        }

        private void cbxTipT_Leave(object sender, EventArgs e)
        {
            to.TIP_TURISTICKOG = cbxTipT.SelectedItem.ToString();
        }

        public void CistiKontrole()
        {
            nudBS.ResetText();
            nupX.ResetText();
            nupY.ResetText();
            dtpEvidencije.ResetText();
            chbxNaseljeno.Checked = false;
            tbxOpstina.ResetText();
            dtpOsnivanja.ResetText();
            chbxTuristicko.Checked = false;
            dtpTur.ResetText();
            cbxTipT.ResetText();
            cbxZnamenitost.Checked = false;
            tbxNazivZnam.ResetText();
            listBox1.Items.Clear();
        }

        private void cbxZnamenitost_CheckedChanged(object sender, EventArgs e)
        {
            if(cbxZnamenitost.Checked)
            {
                tbxNazivZnam.Enabled = true;
                btnDodajZnam.Enabled = true;
                lblZnamenitost.Visible = true;
                listBox1.Visible = true;
            }
            else
            {
                tbxNazivZnam.Enabled = false;
                btnDodajZnam.Enabled = false;
                lblZnamenitost.Visible = false;
                listBox1.Visible = false;
            }
        }

        private void btnDodajZnam_Click(object sender, EventArgs e)
        {
            if(tbxNazivZnam.Text!=null)
            {
                Znamenitost z = new Znamenitost();
                z.NAZIV = tbxNazivZnam.Text;
                z.ID_NASELJENO = to;

                to.ListaZnamenitosti.Add(z);
                listBox1.Items.Clear();
                foreach(Znamenitost zn in to.ListaZnamenitosti)
                {
                    String s = "Naziv: " + zn.NAZIV;
                    listBox1.Items.Add(s);
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell cbc = new DataGridViewCheckBoxCell();
            cbc = (DataGridViewCheckBoxCell)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1];
            if ((Boolean)cbc.Value == false)
            {
                cbc.Value = true;

                Sadrzi novoS = new Sadrzi();

                novoS.ID_TAC_GOBJ = to;

                LinijskiObjekat lo;
                lo = (LinijskiObjekat)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value;

                DTOMenager.DodajSadrziLinijskom(novoS, lo.ID_GOBJ);

                novoS.ID_LIN_GOBJ = lo;

                to.ListaLinijskih.Add(novoS);

                FormSadrziL fsl = new FormSadrziL();
                var rez = fsl.ShowDialog();

                if (rez == DialogResult.OK)
                {
                    novoS.REDNI_BROJ = fsl.rbr;
                    novoS.RASTOJANJE_DO_SLEDECEG = fsl.rastojanjeSl;
                    novoS.RASTOJANJE_DO_PROSLOG = fsl.rastojanjePro;
                }
            }
            else
            {
                cbc.Value = false;

            }
            
        }

        public void PopuniListuL()
        {
            List<LinijskiObjekat> lto = new List<LinijskiObjekat>();

            lto = DTOMenager.CitajLinijske();

            foreach (LinijskiObjekat l in lto)
            {
                dataGridView1.Rows.Add(l.Naziv, false, l);
            }
        }
        
    }
}
