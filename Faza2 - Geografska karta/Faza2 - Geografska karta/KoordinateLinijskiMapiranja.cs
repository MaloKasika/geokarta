﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace Faza2___Geografska_karta
{
    public class KoordinateLinijskiMapiranja:ClassMap<KoordinateLinijski>
    {
        public KoordinateLinijskiMapiranja()
        {
            Table("KOORDINATE_LINIJSKI");

            Id(x => x.ID_KOORD, "ID_KOORD").GeneratedBy.TriggerIdentity();

            Map(x => x.X, "X");
            Map(x => x.Y, "Y");

            References(x => x.ID_GOBJ).Column("ID_GOBJ").LazyLoad();
        }
    }
}
