﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Faza2___Geografska_karta
{
    public partial class FormEditDelete : Form
    {
        GeografskiObjekat geo;
        public FormEditDelete()
        {
            InitializeComponent();
        }

        public FormEditDelete(GeografskiObjekat go)
        {
            this.geo = go;
            InitializeComponent();
        }

        private void FormEditDelete_Load(object sender, EventArgs e)
        {
            tbxNaziv.Text = geo.Naziv;
            lblTip.Text = geo.TIP;
            if(this.geo is LinijskiObjekat)
            {
                LinijskiObjekat lo = (LinijskiObjekat)geo;
                lblDuzina.Visible = true;
                nupDuzina.Visible = true;
                nupDuzina.Value = lo.DUZINA;
                labelnaziv.Text = lo.ID_UZVISENJA.Naziv;
                labelnadmorska.Text = lo.Nadmorska_visina.ToString();
            }
            else if(this.geo is TackastiObjekat)
            {
                TackastiObjekat to = (TackastiObjekat)geo;
                lblKoX.Visible = true;
                lblKoY.Visible = true;
                lblEvidencija.Visible = true;
                dtpEvidencija.Visible = true;
                nupX.Visible = true;
                nupX.Value = (decimal)to.KOORD_X;
                nupY.Visible = true;
                nupY.Value = (decimal)to.KOORD_Y;
                dtpEvidencija.Value = to.DATUM_EVIDENCIJE;
                labelnaziv.Text = to.ID_UZVISENJA.Naziv;
                labelnadmorska.Text = to.Nadmorska_visina.ToString();
            }
            else
            {
                PovrsinskiObjekat po = (PovrsinskiObjekat)geo;
                if (po.VODENE_POVRSINE_FLEG)
                {
                    lblTipV.Visible = true;
                    cbxTip.Visible = true;
                    cbxTip.SelectedItem = po.TIP_VODENIH;
                }
                else
                {
                    lblNadmorska.Visible = true;
                    nupNadV.Visible = true;
                    nupNadV.Value = po.NADMORSKA_VISINA;
                }
                labelnaziv.Text = po.ID_UZVISENJA.Naziv;
                labelnadmorska.Text = po.Nadmorska_visina.ToString();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DTOMenager.Brisi(geo);


            MessageBox.Show("Objekat je obrisan iz baze!");

            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            geo.Naziv = tbxNaziv.Text;

            if(geo is LinijskiObjekat)
            {
                LinijskiObjekat lo = (LinijskiObjekat)geo;
                lo.DUZINA = (int)nupDuzina.Value;

                DTOMenager.Izmeni(lo);
            }
            else if(geo is TackastiObjekat)
            {
                TackastiObjekat to = (TackastiObjekat)geo;
                to.KOORD_X = (int)nupX.Value;
                to.KOORD_Y = (int)nupY.Value;
                to.DATUM_EVIDENCIJE = dtpEvidencija.Value;

                DTOMenager.Izmeni(to);
            }
            else
            {
                PovrsinskiObjekat po = (PovrsinskiObjekat)geo;
                if (po.VODENE_POVRSINE_FLEG)
                {
                    po.TIP_VODENIH = cbxTip.SelectedItem.ToString();
                }
                else
                {
                    po.NADMORSKA_VISINA = (int)nupNadV.Value;
                }

                DTOMenager.Izmeni(po);
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
