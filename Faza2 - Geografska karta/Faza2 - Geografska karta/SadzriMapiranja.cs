﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace Faza2___Geografska_karta
{
    public class SadzriMapiranja:ClassMap<Sadrzi>
    {
        public SadzriMapiranja()
        {
            Table("SADRZI");

            Id(x => x.ID_SADRZI, "ID_SADRZI").GeneratedBy.TriggerIdentity();

            Map(x => x.REDNI_BROJ, "REDNI_BROJ");
            Map(x => x.RASTOJANJE_DO_PROSLOG, "RASTOJANJE_DO_PROSLOG");
            Map(x => x.RASTOJANJE_DO_SLEDECEG, "RASTOJANJE_DO_SLEDECEG");

            References(x => x.ID_LIN_GOBJ).Column("ID_LIN_GOBJ").LazyLoad();
            References(x => x.ID_TAC_GOBJ).Column("ID_TAC_GOBJ").LazyLoad();

        }
    }
}
