﻿namespace Faza2___Geografska_karta
{
    partial class Povrsinski
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblNadV = new System.Windows.Forms.Label();
            this.lblTipV = new System.Windows.Forms.Label();
            this.lblVisnaV = new System.Windows.Forms.Label();
            this.lblVrhI = new System.Windows.Forms.Label();
            this.rbtUzvisenje = new System.Windows.Forms.RadioButton();
            this.rbtVod = new System.Windows.Forms.RadioButton();
            this.cbxTipV = new System.Windows.Forms.ComboBox();
            this.nupNadV = new System.Windows.Forms.NumericUpDown();
            this.tbxImeV = new System.Windows.Forms.TextBox();
            this.nupVisinaV = new System.Windows.Forms.NumericUpDown();
            this.btnDodajVrh = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Naziv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bul = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Lin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nupNadV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupVisinaV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tip površinskog :";
            // 
            // lblNadV
            // 
            this.lblNadV.AutoSize = true;
            this.lblNadV.Location = new System.Drawing.Point(13, 60);
            this.lblNadV.Name = "lblNadV";
            this.lblNadV.Size = new System.Drawing.Size(97, 13);
            this.lblNadV.TabIndex = 3;
            this.lblNadV.Text = "Nadmorska visina :";
            this.lblNadV.Visible = false;
            // 
            // lblTipV
            // 
            this.lblTipV.AutoSize = true;
            this.lblTipV.Location = new System.Drawing.Point(0, 60);
            this.lblTipV.Name = "lblTipV";
            this.lblTipV.Size = new System.Drawing.Size(110, 13);
            this.lblTipV.TabIndex = 4;
            this.lblTipV.Text = "Tip vodene površine :";
            // 
            // lblVisnaV
            // 
            this.lblVisnaV.AutoSize = true;
            this.lblVisnaV.Location = new System.Drawing.Point(45, 125);
            this.lblVisnaV.Name = "lblVisnaV";
            this.lblVisnaV.Size = new System.Drawing.Size(65, 13);
            this.lblVisnaV.TabIndex = 8;
            this.lblVisnaV.Text = "Visina vrha :";
            this.lblVisnaV.Visible = false;
            // 
            // lblVrhI
            // 
            this.lblVrhI.AutoSize = true;
            this.lblVrhI.Location = new System.Drawing.Point(56, 92);
            this.lblVrhI.Name = "lblVrhI";
            this.lblVrhI.Size = new System.Drawing.Size(54, 13);
            this.lblVrhI.TabIndex = 6;
            this.lblVrhI.Text = "Ime vrha :";
            this.lblVrhI.Visible = false;
            // 
            // rbtUzvisenje
            // 
            this.rbtUzvisenje.AutoSize = true;
            this.rbtUzvisenje.Location = new System.Drawing.Point(227, 31);
            this.rbtUzvisenje.Name = "rbtUzvisenje";
            this.rbtUzvisenje.Size = new System.Drawing.Size(71, 17);
            this.rbtUzvisenje.TabIndex = 2;
            this.rbtUzvisenje.Text = "Uzivšenje";
            this.rbtUzvisenje.UseVisualStyleBackColor = true;
            this.rbtUzvisenje.CheckedChanged += new System.EventHandler(this.rbtUzvisenje_CheckedChanged);
            // 
            // rbtVod
            // 
            this.rbtVod.AutoSize = true;
            this.rbtVod.Checked = true;
            this.rbtVod.Location = new System.Drawing.Point(116, 29);
            this.rbtVod.Name = "rbtVod";
            this.rbtVod.Size = new System.Drawing.Size(105, 17);
            this.rbtVod.TabIndex = 1;
            this.rbtVod.TabStop = true;
            this.rbtVod.Text = "Vodena površina";
            this.rbtVod.UseVisualStyleBackColor = true;
            this.rbtVod.CheckedChanged += new System.EventHandler(this.rbtVod_CheckedChanged);
            // 
            // cbxTipV
            // 
            this.cbxTipV.FormattingEnabled = true;
            this.cbxTipV.Items.AddRange(new object[] {
            "Jezero",
            "Bara",
            "More"});
            this.cbxTipV.Location = new System.Drawing.Point(116, 57);
            this.cbxTipV.Name = "cbxTipV";
            this.cbxTipV.Size = new System.Drawing.Size(121, 21);
            this.cbxTipV.TabIndex = 8;
            this.cbxTipV.Leave += new System.EventHandler(this.cbxTipV_Leave);
            // 
            // nupNadV
            // 
            this.nupNadV.Location = new System.Drawing.Point(116, 58);
            this.nupNadV.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
            this.nupNadV.Name = "nupNadV";
            this.nupNadV.Size = new System.Drawing.Size(121, 20);
            this.nupNadV.TabIndex = 5;
            this.nupNadV.Visible = false;
            this.nupNadV.Leave += new System.EventHandler(this.nupNadV_Leave);
            // 
            // tbxImeV
            // 
            this.tbxImeV.Location = new System.Drawing.Point(116, 89);
            this.tbxImeV.Name = "tbxImeV";
            this.tbxImeV.Size = new System.Drawing.Size(121, 20);
            this.tbxImeV.TabIndex = 7;
            this.tbxImeV.Visible = false;
            // 
            // nupVisinaV
            // 
            this.nupVisinaV.Location = new System.Drawing.Point(116, 123);
            this.nupVisinaV.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
            this.nupVisinaV.Name = "nupVisinaV";
            this.nupVisinaV.Size = new System.Drawing.Size(121, 20);
            this.nupVisinaV.TabIndex = 9;
            this.nupVisinaV.Visible = false;
            // 
            // btnDodajVrh
            // 
            this.btnDodajVrh.Location = new System.Drawing.Point(116, 151);
            this.btnDodajVrh.Name = "btnDodajVrh";
            this.btnDodajVrh.Size = new System.Drawing.Size(120, 23);
            this.btnDodajVrh.TabIndex = 10;
            this.btnDodajVrh.Text = "Dodaj vrh";
            this.btnDodajVrh.UseVisualStyleBackColor = true;
            this.btnDodajVrh.Visible = false;
            this.btnDodajVrh.Click += new System.EventHandler(this.btnDodajVrh_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(116, 182);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 11;
            this.listBox1.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Naziv,
            this.Bul,
            this.Lin});
            this.dataGridView1.Location = new System.Drawing.Point(321, 182);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(176, 150);
            this.dataGridView1.TabIndex = 12;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Naziv
            // 
            this.Naziv.HeaderText = "Naziv";
            this.Naziv.Name = "Naziv";
            this.Naziv.ReadOnly = true;
            this.Naziv.Width = 80;
            // 
            // Bul
            // 
            this.Bul.HeaderText = "";
            this.Bul.Name = "Bul";
            this.Bul.ReadOnly = true;
            this.Bul.Width = 30;
            // 
            // Lin
            // 
            this.Lin.HeaderText = "LIn";
            this.Lin.Name = "Lin";
            this.Lin.ReadOnly = true;
            this.Lin.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(303, 156);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(216, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Odaberi linijski od kojih se površinski sastoji :";
            // 
            // Povrsinski
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btnDodajVrh);
            this.Controls.Add(this.nupVisinaV);
            this.Controls.Add(this.tbxImeV);
            this.Controls.Add(this.nupNadV);
            this.Controls.Add(this.cbxTipV);
            this.Controls.Add(this.rbtVod);
            this.Controls.Add(this.rbtUzvisenje);
            this.Controls.Add(this.lblVrhI);
            this.Controls.Add(this.lblVisnaV);
            this.Controls.Add(this.lblTipV);
            this.Controls.Add(this.lblNadV);
            this.Controls.Add(this.label1);
            this.Name = "Povrsinski";
            this.Size = new System.Drawing.Size(552, 453);
            this.Load += new System.EventHandler(this.Povrsinski_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nupNadV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupVisinaV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNadV;
        private System.Windows.Forms.Label lblTipV;
        private System.Windows.Forms.Label lblVisnaV;
        private System.Windows.Forms.Label lblVrhI;
        private System.Windows.Forms.RadioButton rbtUzvisenje;
        private System.Windows.Forms.RadioButton rbtVod;
        private System.Windows.Forms.ComboBox cbxTipV;
        private System.Windows.Forms.NumericUpDown nupNadV;
        private System.Windows.Forms.TextBox tbxImeV;
        private System.Windows.Forms.NumericUpDown nupVisinaV;
        private System.Windows.Forms.Button btnDodajVrh;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Bul;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lin;
        private System.Windows.Forms.Label label2;
    }
}
