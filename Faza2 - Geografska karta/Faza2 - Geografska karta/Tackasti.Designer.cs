﻿namespace Faza2___Geografska_karta
{
    partial class Tackasti
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblBS = new System.Windows.Forms.Label();
            this.lblDO = new System.Windows.Forms.Label();
            this.lblOpstina = new System.Windows.Forms.Label();
            this.chbxNaseljeno = new System.Windows.Forms.CheckBox();
            this.dtpEvidencije = new System.Windows.Forms.DateTimePicker();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.chbxTuristicko = new System.Windows.Forms.CheckBox();
            this.tbxOpstina = new System.Windows.Forms.TextBox();
            this.nudBS = new System.Windows.Forms.NumericUpDown();
            this.dtpOsnivanja = new System.Windows.Forms.DateTimePicker();
            this.dtpTur = new System.Windows.Forms.DateTimePicker();
            this.lblTipT = new System.Windows.Forms.Label();
            this.cbxTipT = new System.Windows.Forms.ComboBox();
            this.nupX = new System.Windows.Forms.NumericUpDown();
            this.nupY = new System.Windows.Forms.NumericUpDown();
            this.cbxZnamenitost = new System.Windows.Forms.CheckBox();
            this.lblNazivZnam = new System.Windows.Forms.Label();
            this.tbxNazivZnam = new System.Windows.Forms.TextBox();
            this.btnDodajZnam = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.lblZnamenitost = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Naziv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Linijski = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.nudBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Koordinata X :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Koordinata Y :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Datum evidencije :";
            // 
            // lblBS
            // 
            this.lblBS.AutoSize = true;
            this.lblBS.Enabled = false;
            this.lblBS.Location = new System.Drawing.Point(25, 164);
            this.lblBS.Name = "lblBS";
            this.lblBS.Size = new System.Drawing.Size(86, 13);
            this.lblBS.TabIndex = 9;
            this.lblBS.Text = "Broj stanovnika :";
            // 
            // lblDO
            // 
            this.lblDO.AutoSize = true;
            this.lblDO.Enabled = false;
            this.lblDO.Location = new System.Drawing.Point(19, 195);
            this.lblDO.Name = "lblDO";
            this.lblDO.Size = new System.Drawing.Size(92, 13);
            this.lblDO.TabIndex = 11;
            this.lblDO.Text = "Datum osnivanja :";
            // 
            // lblOpstina
            // 
            this.lblOpstina.AutoSize = true;
            this.lblOpstina.Enabled = false;
            this.lblOpstina.Location = new System.Drawing.Point(62, 135);
            this.lblOpstina.Name = "lblOpstina";
            this.lblOpstina.Size = new System.Drawing.Size(49, 13);
            this.lblOpstina.TabIndex = 7;
            this.lblOpstina.Text = "Opstina :";
            // 
            // chbxNaseljeno
            // 
            this.chbxNaseljeno.AutoSize = true;
            this.chbxNaseljeno.Location = new System.Drawing.Point(117, 103);
            this.chbxNaseljeno.Name = "chbxNaseljeno";
            this.chbxNaseljeno.Size = new System.Drawing.Size(104, 17);
            this.chbxNaseljeno.TabIndex = 6;
            this.chbxNaseljeno.Text = "Naseljeno mesto";
            this.chbxNaseljeno.UseVisualStyleBackColor = true;
            this.chbxNaseljeno.CheckedChanged += new System.EventHandler(this.cbxNaseljeno_CheckedChanged);
            // 
            // dtpEvidencije
            // 
            this.dtpEvidencije.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEvidencije.Location = new System.Drawing.Point(117, 68);
            this.dtpEvidencije.Name = "dtpEvidencije";
            this.dtpEvidencije.Size = new System.Drawing.Size(107, 20);
            this.dtpEvidencije.TabIndex = 5;
            this.dtpEvidencije.Leave += new System.EventHandler(this.dtpEvidencije_Leave);
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Enabled = false;
            this.lbl2.Location = new System.Drawing.Point(13, 461);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(95, 13);
            this.lbl2.TabIndex = 15;
            this.lbl2.Text = " turističkog mesta :";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Enabled = false;
            this.lbl1.Location = new System.Drawing.Point(13, 448);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(89, 13);
            this.lbl1.TabIndex = 14;
            this.lbl1.Text = "Datum postajanja";
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chbxTuristicko
            // 
            this.chbxTuristicko.AutoSize = true;
            this.chbxTuristicko.Enabled = false;
            this.chbxTuristicko.Location = new System.Drawing.Point(114, 419);
            this.chbxTuristicko.Name = "chbxTuristicko";
            this.chbxTuristicko.Size = new System.Drawing.Size(72, 17);
            this.chbxTuristicko.TabIndex = 13;
            this.chbxTuristicko.Text = "Turističko";
            this.chbxTuristicko.UseVisualStyleBackColor = true;
            this.chbxTuristicko.CheckedChanged += new System.EventHandler(this.chbxTuristicko_CheckedChanged);
            // 
            // tbxOpstina
            // 
            this.tbxOpstina.Enabled = false;
            this.tbxOpstina.Location = new System.Drawing.Point(117, 132);
            this.tbxOpstina.Name = "tbxOpstina";
            this.tbxOpstina.Size = new System.Drawing.Size(107, 20);
            this.tbxOpstina.TabIndex = 8;
            this.tbxOpstina.Leave += new System.EventHandler(this.tbxOpstina_Leave);
            // 
            // nudBS
            // 
            this.nudBS.Enabled = false;
            this.nudBS.Location = new System.Drawing.Point(117, 162);
            this.nudBS.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudBS.Name = "nudBS";
            this.nudBS.Size = new System.Drawing.Size(107, 20);
            this.nudBS.TabIndex = 10;
            this.nudBS.Leave += new System.EventHandler(this.nudBS_Leave);
            // 
            // dtpOsnivanja
            // 
            this.dtpOsnivanja.Enabled = false;
            this.dtpOsnivanja.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOsnivanja.Location = new System.Drawing.Point(117, 189);
            this.dtpOsnivanja.Name = "dtpOsnivanja";
            this.dtpOsnivanja.Size = new System.Drawing.Size(107, 20);
            this.dtpOsnivanja.TabIndex = 12;
            this.dtpOsnivanja.Leave += new System.EventHandler(this.dtpOsnivanja_Leave);
            // 
            // dtpTur
            // 
            this.dtpTur.Enabled = false;
            this.dtpTur.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpTur.Location = new System.Drawing.Point(114, 455);
            this.dtpTur.Name = "dtpTur";
            this.dtpTur.Size = new System.Drawing.Size(107, 20);
            this.dtpTur.TabIndex = 16;
            this.dtpTur.Leave += new System.EventHandler(this.dtpTur_Leave);
            // 
            // lblTipT
            // 
            this.lblTipT.AutoSize = true;
            this.lblTipT.Enabled = false;
            this.lblTipT.Location = new System.Drawing.Point(29, 493);
            this.lblTipT.Name = "lblTipT";
            this.lblTipT.Size = new System.Drawing.Size(79, 13);
            this.lblTipT.TabIndex = 17;
            this.lblTipT.Text = "Tip turističkog :";
            // 
            // cbxTipT
            // 
            this.cbxTipT.Enabled = false;
            this.cbxTipT.FormattingEnabled = true;
            this.cbxTipT.Items.AddRange(new object[] {
            "Banjski",
            "Zimski",
            "Letnji"});
            this.cbxTipT.Location = new System.Drawing.Point(114, 490);
            this.cbxTipT.Name = "cbxTipT";
            this.cbxTipT.Size = new System.Drawing.Size(107, 21);
            this.cbxTipT.TabIndex = 18;
            this.cbxTipT.Leave += new System.EventHandler(this.cbxTipT_Leave);
            // 
            // nupX
            // 
            this.nupX.Location = new System.Drawing.Point(117, 17);
            this.nupX.Name = "nupX";
            this.nupX.Size = new System.Drawing.Size(107, 20);
            this.nupX.TabIndex = 1;
            this.nupX.Leave += new System.EventHandler(this.nupX_Leave);
            // 
            // nupY
            // 
            this.nupY.Location = new System.Drawing.Point(117, 44);
            this.nupY.Name = "nupY";
            this.nupY.Size = new System.Drawing.Size(107, 20);
            this.nupY.TabIndex = 3;
            this.nupY.Leave += new System.EventHandler(this.nupY_Leave);
            // 
            // cbxZnamenitost
            // 
            this.cbxZnamenitost.AutoSize = true;
            this.cbxZnamenitost.Enabled = false;
            this.cbxZnamenitost.Location = new System.Drawing.Point(117, 221);
            this.cbxZnamenitost.Name = "cbxZnamenitost";
            this.cbxZnamenitost.Size = new System.Drawing.Size(113, 17);
            this.cbxZnamenitost.TabIndex = 19;
            this.cbxZnamenitost.Text = "Dodaj znamenitost";
            this.cbxZnamenitost.UseVisualStyleBackColor = true;
            this.cbxZnamenitost.CheckedChanged += new System.EventHandler(this.cbxZnamenitost_CheckedChanged);
            // 
            // lblNazivZnam
            // 
            this.lblNazivZnam.AutoSize = true;
            this.lblNazivZnam.Enabled = false;
            this.lblNazivZnam.Location = new System.Drawing.Point(7, 252);
            this.lblNazivZnam.Name = "lblNazivZnam";
            this.lblNazivZnam.Size = new System.Drawing.Size(101, 13);
            this.lblNazivZnam.TabIndex = 20;
            this.lblNazivZnam.Text = "Naziv znamenitosti :";
            // 
            // tbxNazivZnam
            // 
            this.tbxNazivZnam.Location = new System.Drawing.Point(117, 249);
            this.tbxNazivZnam.Name = "tbxNazivZnam";
            this.tbxNazivZnam.Size = new System.Drawing.Size(107, 20);
            this.tbxNazivZnam.TabIndex = 21;
            // 
            // btnDodajZnam
            // 
            this.btnDodajZnam.Location = new System.Drawing.Point(117, 279);
            this.btnDodajZnam.Name = "btnDodajZnam";
            this.btnDodajZnam.Size = new System.Drawing.Size(107, 23);
            this.btnDodajZnam.TabIndex = 22;
            this.btnDodajZnam.Text = "Dodaj Znamenitost";
            this.btnDodajZnam.UseVisualStyleBackColor = true;
            this.btnDodajZnam.Click += new System.EventHandler(this.btnDodajZnam_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(117, 313);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(105, 95);
            this.listBox1.TabIndex = 23;
            // 
            // lblZnamenitost
            // 
            this.lblZnamenitost.AutoSize = true;
            this.lblZnamenitost.Location = new System.Drawing.Point(40, 307);
            this.lblZnamenitost.Name = "lblZnamenitost";
            this.lblZnamenitost.Size = new System.Drawing.Size(71, 13);
            this.lblZnamenitost.TabIndex = 24;
            this.lblZnamenitost.Text = "Znamenitost :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(278, 189);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Obeleži koje linijske sadrži :";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Naziv,
            this.check,
            this.Linijski});
            this.dataGridView1.Location = new System.Drawing.Point(281, 221);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(176, 150);
            this.dataGridView1.TabIndex = 27;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Naziv
            // 
            this.Naziv.HeaderText = "Naziv";
            this.Naziv.Name = "Naziv";
            this.Naziv.ReadOnly = true;
            this.Naziv.Width = 70;
            // 
            // check
            // 
            this.check.HeaderText = "";
            this.check.Name = "check";
            this.check.Width = 30;
            // 
            // Linijski
            // 
            this.Linijski.HeaderText = "";
            this.Linijski.Name = "Linijski";
            this.Linijski.ReadOnly = true;
            this.Linijski.Visible = false;
            // 
            // Tackasti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblZnamenitost);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btnDodajZnam);
            this.Controls.Add(this.tbxNazivZnam);
            this.Controls.Add(this.lblNazivZnam);
            this.Controls.Add(this.cbxZnamenitost);
            this.Controls.Add(this.nupY);
            this.Controls.Add(this.nupX);
            this.Controls.Add(this.cbxTipT);
            this.Controls.Add(this.lblTipT);
            this.Controls.Add(this.dtpTur);
            this.Controls.Add(this.dtpOsnivanja);
            this.Controls.Add(this.nudBS);
            this.Controls.Add(this.tbxOpstina);
            this.Controls.Add(this.chbxTuristicko);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.dtpEvidencije);
            this.Controls.Add(this.chbxNaseljeno);
            this.Controls.Add(this.lblOpstina);
            this.Controls.Add(this.lblDO);
            this.Controls.Add(this.lblBS);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Tackasti";
            this.Size = new System.Drawing.Size(501, 558);
            ((System.ComponentModel.ISupportInitialize)(this.nudBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblBS;
        private System.Windows.Forms.Label lblDO;
        private System.Windows.Forms.Label lblOpstina;
        private System.Windows.Forms.CheckBox chbxNaseljeno;
        private System.Windows.Forms.DateTimePicker dtpEvidencije;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.CheckBox chbxTuristicko;
        private System.Windows.Forms.TextBox tbxOpstina;
        private System.Windows.Forms.NumericUpDown nudBS;
        private System.Windows.Forms.DateTimePicker dtpOsnivanja;
        private System.Windows.Forms.DateTimePicker dtpTur;
        private System.Windows.Forms.Label lblTipT;
        private System.Windows.Forms.ComboBox cbxTipT;
        private System.Windows.Forms.NumericUpDown nupX;
        private System.Windows.Forms.NumericUpDown nupY;
        private System.Windows.Forms.CheckBox cbxZnamenitost;
        private System.Windows.Forms.Label lblNazivZnam;
        private System.Windows.Forms.TextBox tbxNazivZnam;
        private System.Windows.Forms.Button btnDodajZnam;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label lblZnamenitost;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn Linijski;
    }
}
