﻿using System;
using FluentNHibernate.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class LinijskiObjekatMapiranja : SubclassMap<LinijskiObjekat>
    {
        public LinijskiObjekatMapiranja()
        {
            Table("LINIJSKI_OBJEKAT");

            KeyColumn("ID_GOBJ");

            Map(x => x.DUZINA).Column("DUZINA");
            Map(x => x.REKA_FLEG).Column("REKA_FLEG").Nullable();
            Map(x => x.PUT_FLEG).Column("PUT_FLEG").Nullable();
            Map(x => x.KLASA_PUTA).Column("KLASA_PUTA").Nullable();
            Map(x => x.GRANICNA_LINIJA_FLEG).Column("GRANICNA_LINIJA_FLEG").Nullable();
            Map(x => x.IME_DRZAVE).Column("IME_DRZAVE").Nullable();

   //         HasMany(x => x.ListaPovrsinskih).KeyColumn("ID_LIN_GOBJ").LazyLoad().Cascade.All().Inverse();
   //         HasMany(x => x.ListaTackastih).KeyColumn("ID_LIN_GOBJ").LazyLoad().Cascade.All().Inverse();
   //         HasMany(x => x.ListaKoordinata).KeyColumn("ID_GOBJ").LazyLoad().Cascade.All().Inverse();
        }
    }
}
