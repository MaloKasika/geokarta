﻿namespace Faza2___Geografska_karta
{
    partial class FormApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtnLin = new System.Windows.Forms.RadioButton();
            this.rbtnTac = new System.Windows.Forms.RadioButton();
            this.rbtnPov = new System.Windows.Forms.RadioButton();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnJos = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxNaziv = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgv1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Naziv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Obj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtgPov = new System.Windows.Forms.DataGridView();
            this.NazivUzv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NadVisina = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Povrsinski = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.linijski1 = new Faza2___Geografska_karta.Linijski();
            this.povrsinski1 = new Faza2___Geografska_karta.Povrsinski();
            this.tackasti1 = new Faza2___Geografska_karta.Tackasti();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPov)).BeginInit();
            this.SuspendLayout();
            // 
            // rbtnLin
            // 
            this.rbtnLin.AutoSize = true;
            this.rbtnLin.Checked = true;
            this.rbtnLin.Location = new System.Drawing.Point(179, 61);
            this.rbtnLin.Name = "rbtnLin";
            this.rbtnLin.Size = new System.Drawing.Size(56, 17);
            this.rbtnLin.TabIndex = 3;
            this.rbtnLin.TabStop = true;
            this.rbtnLin.Text = "Linijski";
            this.rbtnLin.UseVisualStyleBackColor = true;
            this.rbtnLin.CheckedChanged += new System.EventHandler(this.rbtnLin_CheckedChanged);
            // 
            // rbtnTac
            // 
            this.rbtnTac.AutoSize = true;
            this.rbtnTac.Location = new System.Drawing.Point(254, 61);
            this.rbtnTac.Name = "rbtnTac";
            this.rbtnTac.Size = new System.Drawing.Size(66, 17);
            this.rbtnTac.TabIndex = 4;
            this.rbtnTac.Text = "Tackasti";
            this.rbtnTac.UseVisualStyleBackColor = true;
            this.rbtnTac.CheckedChanged += new System.EventHandler(this.rbtnTac_CheckedChanged);
            // 
            // rbtnPov
            // 
            this.rbtnPov.AutoSize = true;
            this.rbtnPov.Location = new System.Drawing.Point(334, 61);
            this.rbtnPov.Name = "rbtnPov";
            this.rbtnPov.Size = new System.Drawing.Size(73, 17);
            this.rbtnPov.TabIndex = 5;
            this.rbtnPov.Text = "Povrsinski";
            this.rbtnPov.UseVisualStyleBackColor = true;
            this.rbtnPov.CheckedChanged += new System.EventHandler(this.rbtnPov_CheckedChanged);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(390, 658);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 23);
            this.btnDodaj.TabIndex = 11;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnJos
            // 
            this.btnJos.Location = new System.Drawing.Point(471, 658);
            this.btnJos.Name = "btnJos";
            this.btnJos.Size = new System.Drawing.Size(75, 23);
            this.btnJos.TabIndex = 12;
            this.btnJos.Text = "Lista >>";
            this.btnJos.UseVisualStyleBackColor = true;
            this.btnJos.Click += new System.EventHandler(this.btnJos_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(720, 658);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 13;
            this.button3.Text = "<< Sakrij";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(120, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Naziv :";
            // 
            // tbxNaziv
            // 
            this.tbxNaziv.Location = new System.Drawing.Point(179, 12);
            this.tbxNaziv.Name = "tbxNaziv";
            this.tbxNaziv.Size = new System.Drawing.Size(228, 20);
            this.tbxNaziv.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(137, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tip :";
            // 
            // dgv1
            // 
            this.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Naziv,
            this.Tip,
            this.Obj});
            this.dgv1.Location = new System.Drawing.Point(566, 15);
            this.dgv1.Name = "dgv1";
            this.dgv1.Size = new System.Drawing.Size(229, 627);
            this.dgv1.TabIndex = 13;
            this.dgv1.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseDoubleClick);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Width = 30;
            // 
            // Naziv
            // 
            this.Naziv.HeaderText = "Naziv";
            this.Naziv.Name = "Naziv";
            this.Naziv.Width = 115;
            // 
            // Tip
            // 
            this.Tip.HeaderText = "Tip";
            this.Tip.Name = "Tip";
            this.Tip.Width = 25;
            // 
            // Obj
            // 
            this.Obj.HeaderText = "Obj";
            this.Obj.Name = "Obj";
            this.Obj.Visible = false;
            // 
            // dtgPov
            // 
            this.dtgPov.AllowUserToAddRows = false;
            this.dtgPov.AllowUserToDeleteRows = false;
            this.dtgPov.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPov.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NazivUzv,
            this.NadVisina,
            this.Povrsinski});
            this.dtgPov.Location = new System.Drawing.Point(340, 125);
            this.dtgPov.Name = "dtgPov";
            this.dtgPov.ReadOnly = true;
            this.dtgPov.Size = new System.Drawing.Size(176, 150);
            this.dtgPov.TabIndex = 10;
            this.dtgPov.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPov_CellContentClick);
            // 
            // NazivUzv
            // 
            this.NazivUzv.HeaderText = "Naziv Uzvisenja";
            this.NazivUzv.Name = "NazivUzv";
            this.NazivUzv.ReadOnly = true;
            // 
            // NadVisina
            // 
            this.NadVisina.HeaderText = "";
            this.NadVisina.Name = "NadVisina";
            this.NadVisina.ReadOnly = true;
            this.NadVisina.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NadVisina.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.NadVisina.Width = 30;
            // 
            // Povrsinski
            // 
            this.Povrsinski.HeaderText = "IDPov";
            this.Povrsinski.Name = "Povrsinski";
            this.Povrsinski.ReadOnly = true;
            this.Povrsinski.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(341, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Izaberi na kom uzvišenju se nalazi :";
            // 
            // linijski1
            // 
            this.linijski1.Location = new System.Drawing.Point(75, 94);
            this.linijski1.Name = "linijski1";
            this.linijski1.Size = new System.Drawing.Size(454, 558);
            this.linijski1.TabIndex = 6;
            // 
            // povrsinski1
            // 
            this.povrsinski1.Location = new System.Drawing.Point(18, 126);
            this.povrsinski1.Name = "povrsinski1";
            this.povrsinski1.Size = new System.Drawing.Size(521, 355);
            this.povrsinski1.TabIndex = 8;
            this.povrsinski1.Visible = false;
            // 
            // tackasti1
            // 
            this.tackasti1.Location = new System.Drawing.Point(59, 98);
            this.tackasti1.Name = "tackasti1";
            this.tackasti1.Size = new System.Drawing.Size(469, 554);
            this.tackasti1.TabIndex = 7;
            this.tackasti1.Visible = false;
            // 
            // FormApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 697);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtgPov);
            this.Controls.Add(this.dgv1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxNaziv);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnJos);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.rbtnPov);
            this.Controls.Add(this.rbtnTac);
            this.Controls.Add(this.rbtnLin);
            this.Controls.Add(this.linijski1);
            this.Controls.Add(this.povrsinski1);
            this.Controls.Add(this.tackasti1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(823, 725);
            this.MinimumSize = new System.Drawing.Size(574, 725);
            this.Name = "FormApp";
            this.ShowIcon = false;
            this.Text = "Geografska Karta";
            this.Load += new System.EventHandler(this.FormApp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPov)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtnLin;
        private System.Windows.Forms.RadioButton rbtnTac;
        private System.Windows.Forms.RadioButton rbtnPov;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnJos;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxNaziv;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgv1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tip;
        private System.Windows.Forms.DataGridViewTextBoxColumn Obj;
        private Linijski linijski1;
        private Povrsinski povrsinski1;
        private Tackasti tackasti1;
        private System.Windows.Forms.DataGridView dtgPov;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivUzv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn NadVisina;
        private System.Windows.Forms.DataGridViewTextBoxColumn Povrsinski;
        private System.Windows.Forms.Label label3;
    }
}