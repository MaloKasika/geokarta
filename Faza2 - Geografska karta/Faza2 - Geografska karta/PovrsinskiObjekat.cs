﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class PovrsinskiObjekat : GeografskiObjekat
    {
        public virtual bool VODENE_POVRSINE_FLEG { get; set; }
        public virtual string TIP_VODENIH { get; set; }
        public virtual bool UZVISENJE_FLEG { get; set; }
        public virtual int NADMORSKA_VISINA { get; set; }

        public virtual IList<GeografskiObjekat> ListaGeografskihObjekata { get; set; }
        public virtual IList<Vrh> ListaVrhova { get; set; }
        public virtual IList<SastojiSeOd> ListaLinijskih { get; set; }

        public PovrsinskiObjekat()
        {
            this.TIP = "P";

            this.UZVISENJE_FLEG = false;
            this.VODENE_POVRSINE_FLEG = false;

            ListaGeografskihObjekata = new List<GeografskiObjekat>();
            ListaVrhova = new List<Vrh>();
            ListaLinijskih = new List<SastojiSeOd>();
        }
    }
}
