﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class Vrh
    {
        public virtual int ID_VRH { get; set; }
        public virtual string IME { get; set; }
        public virtual int VISINA { get; set; }
        public virtual PovrsinskiObjekat ID_UZVISENJA { get; set; }

        public Vrh()
        {
            
        }
    }
}
