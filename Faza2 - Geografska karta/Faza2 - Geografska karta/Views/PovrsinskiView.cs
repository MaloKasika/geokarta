﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta.Views
{
    public class PovrsinskiView
    {
        public int ID_GOBJ { get; set; }
        public bool VODENE_POVRSINE_FLEG { get; set; }
        public string TIP_VODENIH { get; set; }
        public bool UZVISENJE_FLEG { get; set; }
        public int NADMORSKA_VISINA { get; set; }

        public IList<GeografskiObjekat> ListaGeografskihObjekata { get; set; }
        public IList<Vrh> ListaVrhova { get; set; }
        public IList<SastojiSeOd> ListaLinijskih { get; set; }
           
        public PovrsinskiView(PovrsinskiObjekat p)
        {
            this.ListaGeografskihObjekata = p.ListaGeografskihObjekata;
            this.ListaVrhova = p.ListaVrhova;
            this.ListaLinijskih = p.ListaLinijskih;

            this.ID_GOBJ = p.ID_GOBJ;
            this.VODENE_POVRSINE_FLEG = p.VODENE_POVRSINE_FLEG;
            this.TIP_VODENIH = p.TIP_VODENIH;
            this.UZVISENJE_FLEG = p.UZVISENJE_FLEG;
            this.NADMORSKA_VISINA = p.NADMORSKA_VISINA;
        }

        public PovrsinskiView()
        {

        }
    }
}
