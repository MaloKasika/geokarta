﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta.Views
{
    public class VrhView
    {
        public int ID_VRH { get; set; }
        public string IME { get; set; }
        public int VISINA { get; set; }

        public VrhView(Vrh v)
        {
            this.ID_VRH = v.ID_VRH;
            this.IME = v.IME;
            this.VISINA = v.VISINA;
        }

        public VrhView()
        {

        }
    }
}
