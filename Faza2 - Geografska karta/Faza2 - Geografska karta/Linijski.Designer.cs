﻿namespace Faza2___Geografska_karta
{
    partial class Linijski
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rbtnReka = new System.Windows.Forms.RadioButton();
            this.rbtnGran = new System.Windows.Forms.RadioButton();
            this.rbtnPut = new System.Windows.Forms.RadioButton();
            this.cmbKlase = new System.Windows.Forms.ComboBox();
            this.tbxImeD = new System.Windows.Forms.TextBox();
            this.lblKlasa = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDodajKoo = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.lblImeD = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nupY = new System.Windows.Forms.NumericUpDown();
            this.nupX = new System.Windows.Forms.NumericUpDown();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Naziv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sadrzi = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Tackasti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.NazivP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moze = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Pov = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dužina :";
            // 
            // rbtnReka
            // 
            this.rbtnReka.AutoSize = true;
            this.rbtnReka.Checked = true;
            this.rbtnReka.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnReka.Location = new System.Drawing.Point(99, 53);
            this.rbtnReka.Name = "rbtnReka";
            this.rbtnReka.Size = new System.Drawing.Size(51, 17);
            this.rbtnReka.TabIndex = 3;
            this.rbtnReka.TabStop = true;
            this.rbtnReka.Text = "Reka";
            this.rbtnReka.UseVisualStyleBackColor = true;
            this.rbtnReka.CheckedChanged += new System.EventHandler(this.rbtnReka_CheckedChanged);
            // 
            // rbtnGran
            // 
            this.rbtnGran.AutoSize = true;
            this.rbtnGran.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnGran.Location = new System.Drawing.Point(203, 53);
            this.rbtnGran.Name = "rbtnGran";
            this.rbtnGran.Size = new System.Drawing.Size(62, 17);
            this.rbtnGran.TabIndex = 5;
            this.rbtnGran.Text = "Granica";
            this.rbtnGran.UseVisualStyleBackColor = true;
            this.rbtnGran.CheckedChanged += new System.EventHandler(this.rbtnGran_CheckedChanged);
            // 
            // rbtnPut
            // 
            this.rbtnPut.AutoSize = true;
            this.rbtnPut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnPut.Location = new System.Drawing.Point(156, 53);
            this.rbtnPut.Name = "rbtnPut";
            this.rbtnPut.Size = new System.Drawing.Size(41, 17);
            this.rbtnPut.TabIndex = 4;
            this.rbtnPut.Text = "Put";
            this.rbtnPut.UseVisualStyleBackColor = true;
            this.rbtnPut.CheckedChanged += new System.EventHandler(this.rbtnPut_CheckedChanged);
            // 
            // cmbKlase
            // 
            this.cmbKlase.FormattingEnabled = true;
            this.cmbKlase.Items.AddRange(new object[] {
            "1.",
            "2.",
            "3.",
            "auto-put"});
            this.cmbKlase.Location = new System.Drawing.Point(99, 78);
            this.cmbKlase.Name = "cmbKlase";
            this.cmbKlase.Size = new System.Drawing.Size(121, 21);
            this.cmbKlase.TabIndex = 5;
            this.cmbKlase.Visible = false;
            this.cmbKlase.Leave += new System.EventHandler(this.cmbKlase_Leave);
            // 
            // tbxImeD
            // 
            this.tbxImeD.Location = new System.Drawing.Point(99, 78);
            this.tbxImeD.Name = "tbxImeD";
            this.tbxImeD.Size = new System.Drawing.Size(121, 20);
            this.tbxImeD.TabIndex = 7;
            this.tbxImeD.Visible = false;
            this.tbxImeD.Leave += new System.EventHandler(this.tbxImeD_Leave);
            // 
            // lblKlasa
            // 
            this.lblKlasa.AutoSize = true;
            this.lblKlasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKlasa.Location = new System.Drawing.Point(30, 81);
            this.lblKlasa.Name = "lblKlasa";
            this.lblKlasa.Size = new System.Drawing.Size(63, 13);
            this.lblKlasa.TabIndex = 9;
            this.lblKlasa.Text = "Klasa puta :";
            this.lblKlasa.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tip linijskog :";
            // 
            // btnDodajKoo
            // 
            this.btnDodajKoo.Location = new System.Drawing.Point(99, 169);
            this.btnDodajKoo.Name = "btnDodajKoo";
            this.btnDodajKoo.Size = new System.Drawing.Size(121, 23);
            this.btnDodajKoo.TabIndex = 12;
            this.btnDodajKoo.Text = "Dodaj koordinate";
            this.btnDodajKoo.UseVisualStyleBackColor = true;
            this.btnDodajKoo.Click += new System.EventHandler(this.btnDodajKoo_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(99, 208);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(121, 95);
            this.listBox1.TabIndex = 14;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(99, 22);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(121, 20);
            this.numericUpDown1.TabIndex = 1;
            this.numericUpDown1.Leave += new System.EventHandler(this.numericUpDown1_Leave);
            // 
            // lblImeD
            // 
            this.lblImeD.AutoSize = true;
            this.lblImeD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImeD.Location = new System.Drawing.Point(28, 82);
            this.lblImeD.Name = "lblImeD";
            this.lblImeD.Size = new System.Drawing.Size(65, 13);
            this.lblImeD.TabIndex = 6;
            this.lblImeD.Text = "Ime države :";
            this.lblImeD.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Koordinata Y :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Koordinata X :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(29, 208);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Koordinate :";
            // 
            // nupY
            // 
            this.nupY.Location = new System.Drawing.Point(99, 144);
            this.nupY.Name = "nupY";
            this.nupY.Size = new System.Drawing.Size(121, 20);
            this.nupY.TabIndex = 11;
            // 
            // nupX
            // 
            this.nupX.Location = new System.Drawing.Point(99, 111);
            this.nupX.Name = "nupX";
            this.nupX.Size = new System.Drawing.Size(121, 20);
            this.nupX.TabIndex = 9;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Naziv,
            this.Sadrzi,
            this.Tackasti});
            this.dataGridView1.Location = new System.Drawing.Point(264, 397);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(176, 150);
            this.dataGridView1.TabIndex = 17;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Naziv
            // 
            this.Naziv.HeaderText = "Naziv";
            this.Naziv.Name = "Naziv";
            this.Naziv.ReadOnly = true;
            this.Naziv.Width = 80;
            // 
            // Sadrzi
            // 
            this.Sadrzi.HeaderText = "";
            this.Sadrzi.Name = "Sadrzi";
            this.Sadrzi.ReadOnly = true;
            this.Sadrzi.Width = 30;
            // 
            // Tackasti
            // 
            this.Tackasti.HeaderText = "Tackasti";
            this.Tackasti.Name = "Tackasti";
            this.Tackasti.ReadOnly = true;
            this.Tackasti.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(271, 375);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Obeleži koje tačkaste sadrži :";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NazivP,
            this.Moze,
            this.Pov});
            this.dataGridView2.Location = new System.Drawing.Point(264, 219);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(176, 150);
            this.dataGridView2.TabIndex = 19;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // NazivP
            // 
            this.NazivP.HeaderText = "Naziv";
            this.NazivP.Name = "NazivP";
            this.NazivP.ReadOnly = true;
            this.NazivP.Width = 80;
            // 
            // Moze
            // 
            this.Moze.HeaderText = "";
            this.Moze.Name = "Moze";
            this.Moze.ReadOnly = true;
            this.Moze.Width = 30;
            // 
            // Pov
            // 
            this.Pov.HeaderText = "";
            this.Pov.Name = "Pov";
            this.Pov.ReadOnly = true;
            this.Pov.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(267, 197);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(171, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Obeleži kojim površinskim pripada :";
            // 
            // Linijski
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.nupX);
            this.Controls.Add(this.nupY);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblImeD);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btnDodajKoo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblKlasa);
            this.Controls.Add(this.tbxImeD);
            this.Controls.Add(this.cmbKlase);
            this.Controls.Add(this.rbtnPut);
            this.Controls.Add(this.rbtnGran);
            this.Controls.Add(this.rbtnReka);
            this.Controls.Add(this.label1);
            this.Name = "Linijski";
            this.Size = new System.Drawing.Size(452, 565);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtnReka;
        private System.Windows.Forms.RadioButton rbtnGran;
        private System.Windows.Forms.RadioButton rbtnPut;
        private System.Windows.Forms.ComboBox cmbKlase;
        private System.Windows.Forms.TextBox tbxImeD;
        private System.Windows.Forms.Label lblKlasa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDodajKoo;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label lblImeD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nupY;
        private System.Windows.Forms.NumericUpDown nupX;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Sadrzi;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tackasti;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivP;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Moze;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pov;
        private System.Windows.Forms.Label label7;
    }
}
