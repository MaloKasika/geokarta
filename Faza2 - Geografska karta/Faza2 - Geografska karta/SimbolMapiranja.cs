﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class SimbolMapiranja : ClassMap<Simbol>
    {
        public SimbolMapiranja()
        {
            Table("SIMBOL");

            Id(x => x.ID_SIMBOL, "ID_SIMBOL").GeneratedBy.TriggerIdentity();

            Map(x => x.OD_BROJ_STANOVNIKA).Column("OD_BROJ_STANOVNIKA");
            Map(x => x.DO_BROJ_STANOVNIKA).Column("DO_BROJ_STANOVNIKA").Nullable();
            Map(x => x.NAZIV).Column("NAZIV").Nullable();

      //      HasMany(x => x.ListaNaseljenihMesta).KeyColumn("ID_SIMBOLA").LazyLoad().Cascade.All().Inverse();
        }
    }
}
