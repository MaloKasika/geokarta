﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using System.Configuration;

namespace Faza2___Geografska_karta
{
    class DataLayer
    {
        private static ISessionFactory _factory = null;
        private static object kljuc = new object();

        public static ISession GetSession()
        {
            if (_factory == null)
            {
                lock (kljuc)
                {
                    if (_factory == null)
                    {
                        _factory = CreateSessionFactory();
                    }
                }
            }
            return _factory.OpenSession();
        }

        private static ISessionFactory CreateSessionFactory()
        {
            try
            {
                var cfg = OracleManagedDataClientConfiguration.Oracle10
                .ConnectionString(c =>
                c.Is("Data Source=gislab-oracle.elfak.ni.ac.rs:1521/SBP_PDB;User Id=S15387;Password=Sreda15okt"));

                return Fluently.Configure()
                  .Database(cfg.ShowSql())
                  .Mappings(m => m.FluentMappings.AddFromAssemblyOf<GeograskiObjekatMapiranja>())
                  .BuildSessionFactory();
            }
            catch (Exception ec)
            {
                System.Windows.Forms.MessageBox.Show(ec.Message);
                return null;
            }
        }
    }
}
